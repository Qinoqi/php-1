<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 1 PHP</title>
</head>
<body>
    <h1>Contoh String</h1>
    <?php
    $string = "HELLO WORLD ARIF NOKI ASARI";
    $panjangkalimat = strlen($string);
    $jumlahkata = str_word_count($string);
    echo " Kalimat : $string <br>";
    echo " Panjang Kalimat : $panjangkalimat <br>";
    echo " Jumlah Kata : $jumlahkata <br>";

    echo "<h2>Contoh Ke 2</h2>";
    $string1 = "Hello World";
    echo " Kalimat : $string1 <br>";
    echo " Kata Pertama : ".substr($string1, 0, 5)."<br>";
    echo " Kata Kedua : ".substr($string1, 6, 5);

    echo "<h2>Contoh Ke 3</h2>";
    $string2 = "Nama Saya Noqi";
    echo " Kalimat : $string2 <br>";
    echo " Kalimat String Diganti : ".str_replace("Noqi","Asari",$string2);
    ?>
</body>
</html>