<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh Array 1</h2>
    <?php
    echo "<h3>Contoh Array 1</h3>";
    $kids = array ("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven");
    $adults = array ("Hopper", "Nancy", "Joyce", "Jonathan", "Murray");
    print_r ($kids);

    echo "<h3>Contoh Array 2</h3>";
    echo " Total Array :" .count($kids);
    echo "<ul>";
    echo "<li>" .$kids[0] . "</li>";
    echo "<li>" .$kids[1] . "</li>";
    echo "<li>" .$kids[2] . "</li>";
    echo "</ul>";

    echo "<h3>Contoh Array 3</h3>";
    $biodata = [
     ["Nama"=>"Will Byers", "Umur" => 12, "Aliases" => "Will the Wise","Status"=>"Alive"],
     ["Nama"=>"Mike Wheeler", "Umur" => 12, "Aliases" => "Dungeon Master","Status" => "Alive"],
     ["Nama"=>"Jim Hopper", "Umur" => 43, "Aliases" => "Chief Hopper", "Status" => "Deceased"],
     ["Nama"=>"Eleven", "Umur" => 12, "Aliases" => "El", "Status" => "Alive"],
    ];
    echo "<pre>";
    print_r($biodata);
    echo "</pre>";

    
    ?>
</body>
</html>